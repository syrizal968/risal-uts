<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\facades\DB;

class TblguruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tblgurus')->insert([
            'Nama' => 'andi',
            'Jeniskelamin' => 'pria',
            'Gelar' => 'spdi',
            'Notelepon' => '083667449700',
        ]);
    }
}
