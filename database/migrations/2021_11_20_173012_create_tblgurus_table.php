<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblgurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblgurus', function (Blueprint $table) {
            $table->id();
            $table->string('Nama');
            $table->enum('Jeniskelamin', ['pria', 'wanita']);
            $table->string('Gelar');
            $table->biginteger('Notelepon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblgurus');
    }
}
