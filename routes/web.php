<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuruController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GuruController::class, 'home']);
Route::get('/siswa', [GuruController::class, 'siswa']);
Route::get('/user', [GuruController::class, 'user']);
Route::get('/tambah', [GuruController::class, 'tambah']);

Route::post("/gurupost", [GuruController::class, 'gurupost']);

Route::get('/guru', [GuruController::class, 'index'])->name('guru');

Route::get('/databaru/{id}', [GuruController::class, 'databaru'])->name('databaru');
Route::post('/updatedata/{id}', [GuruController::class, 'updatedata'])->name('/updatedata');
