@extends('layout.y-template')
@section('title','Guru')
@section('content')

  </head>
  <body>
    
    <h1 class="text-center mb-3">DATA GURU</h1>

      <div class="container">
        <a href="/tambah" class="btn btn-primary">Tambah +</a>
          <div class="row">
            @if ($message=Session::get('succes'))
                <div class="alert alert-primary" role="alert">
                {{$message}}
                </div>
            @endif
  <table class="table table-striped table-hover">
  <thead class="table-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Jenis Kelamin</th>
      <th scope="col">Gelar</th>
      <th scope="col">No Telepon</th>
      <th scope="col">Dibuat</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $row)
    <tr>
      <th scope="row">{{ $row ->id }} </th>
      <td>{{ $row->Nama }} </td>
      <td>{{ $row->Jeniskelamin }} </td>
      <td>{{ $row->Gelar }} </td>
      <td>0{{ $row->Notelepon }} </td>
      <td>{{ $row->created_at->diffForHumans()}} </td>
      <td>
        <a href="/databaru/{{$row->id}}" class="btn btn-success">Edit</a>
        <a href="/t" class="btn btn-danger">Hapus</a>
      </td>
      
    </tr>
    @endforeach
    </tbody>
  </table>
    </div>
    </div>

    
  </body>




@endsection

