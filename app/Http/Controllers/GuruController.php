<?php

namespace App\Http\Controllers;

use App\Models\Tblguru;
use Illuminate\Http\Request;

use phpDocumentor\Reflection\Types\This;

class GuruController extends Controller
{

    public function index()
    {
        $data = Tblguru::all();
        return view('y-guru', compact('data'));
    }

    public function gurupost(request $request)
    {
        Tblguru::create($request->all());
        return redirect()->route('guru')->with('success', 'Data Telah Ditambahkan');
    }

    public function databaru($id)
    {
        $data = Tblguru::find($id);
        return view('databaru', compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = Tblguru::find($id);
        $data->update($request->all());
        return redirect()->route('guru')->with('success', 'Data Telah Ditambahkan');
    }

    public function user()
    {
        return view('y-user');
    }

    public function siswa()
    {
        return view('y-siswa');
    }

    public function home()
    {
        return view('y-home');
    }

    public function tambah()
    {
        return view('tambah');
    }
}
